import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TransferBoxComponent } from './components/transfer-box/transfer-box.component';
import { TransactionsBoxComponent } from './components/transactions-box/transactions-box.component';
import { HttpClientModule } from '@angular/common/http';
import { AmountCurrencyPipe } from './pipes/amount-currency.pipe';
import { BeneficiaryImagePipe } from './pipes/beneficiary-image.pipe';
import { PreviewPanelComponent } from './components/preview-panel/preview-panel.component';
import { HandleNoImageDirective } from './directives/handle-no-image.directive';

@NgModule({
  declarations: [
    AppComponent,
    TransferBoxComponent,
    TransactionsBoxComponent,
    AmountCurrencyPipe,
    BeneficiaryImagePipe,
    PreviewPanelComponent,
    HandleNoImageDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
