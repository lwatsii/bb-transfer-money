import { Pipe, PipeTransform } from '@angular/core';
import { AmountCurrency } from '../services/model';

function formatAmount(amount: number): string {
  // TODO: analyze requirements to check if default currency formatter can be used here
  return amount.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })
}

const formatters = {
  'USD': ({ amount }: AmountCurrency) => {
    return ( Math.sign(amount) === -1 ? '-' : '' ) + '$' + formatAmount(Math.abs(amount));
  },
  'EUR': ({ amount }: AmountCurrency) => {
    return ( Math.sign(amount) === -1 ? '-' : '' ) + '€' + formatAmount(Math.abs(amount));
  },
  'PLN': ({ amount }: AmountCurrency) => {
    return `${formatAmount(amount)} zł`;
  }
};

function defaultFormatter({amount, currencyCode}: AmountCurrency) {
  return `${amount} ${currencyCode}`;
}

@Pipe({
  name: 'amountCurrency'
})
export class AmountCurrencyPipe implements PipeTransform {

  transform(value: AmountCurrency | null): string {
    if (value) {
      const formatter = formatters[value.currencyCode] || defaultFormatter;
      return formatter(value);
    } else {
      return '';
    }
  }

}
