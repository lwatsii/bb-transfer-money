import { stringify } from '@angular/compiler/src/util';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'beneficiaryImage'
})
export class BeneficiaryImagePipe implements PipeTransform {

  transform(value: string): string {
    return 'assets/beneficiary/' + value.replace(/[ ]/g, '-').toLowerCase() + '.png';
  }

}
