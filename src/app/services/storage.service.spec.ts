import { TestBed } from '@angular/core/testing';
import { StorageService } from './storage.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

const dummyTransactions = {
  data: [
    {
      categoryCode: '#12a580',
      dates: {
        valueDate: 1600493600000,
      },
      transaction: {
        amountCurrency: {
          amount: 5000,
          currencyCode: 'EUR',
        },
        type: 'Salaries',
        creditDebitIndicator: 'CRDT',
      },
      merchant: {
        name: 'Backbase',
        accountNumber: 'SI64397745065188826',
      },
    },
    {
      categoryCode: '#12a580',
      dates: {
        valueDate: 1600387200000,
      },
      transaction: {
        amountCurrency: {
          amount: '82.02',
          currencyCode: 'EUR',
        },
        type: 'Card Payment',
        creditDebitIndicator: 'DBIT',
      },
      merchant: {
        name: 'The Tea Lounge',
        accountNumber: 'SI64397745065188826',
      },
    },
  ],
};

fdescribe('StorageService', () => {
  let service: StorageService;

  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(StorageService);

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve transactions from backend', () => {
    service.getTransactions().subscribe(t => {
      expect(t.length).toBe(2);
    });

    const req = httpTestingController.expectOne('assets/mock/transactions.json');
    expect(req.request.method).toEqual('GET');
    req.flush(dummyTransactions);
  });
});
