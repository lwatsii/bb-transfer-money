import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LocalStorageService } from './local-storage.service';
import { AmountCurrency, Transaction } from './model';
import { StorageService } from './storage.service';

const ZERO: AmountCurrency = {
  amount: 0,
  currencyCode: ''
}

const STORAGE_KEY = 'transactions';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  private transactions = new BehaviorSubject<Transaction[]>([]);
  public transactions$ = this.transactions.asObservable();

  private balance = new BehaviorSubject<AmountCurrency>(ZERO);
  public balance$ = this.balance.asObservable();

  constructor(
    private storage: StorageService,
    private localStorage: LocalStorageService,
    ) {
    // Initialize transactions
    if (this.localStorage.get(STORAGE_KEY)) {
      this.transactions.next(
        (this.localStorage.get(STORAGE_KEY) as Transaction[])
        .map(t => {
          if (typeof t.date === 'string') {
            t.date = new Date(Date.parse(t.date));
          }
          return t;
        })
        );
      this.calcBalance();
  } else {
      storage.getTransactions().subscribe(transactions => {
        this.transactions.next(transactions);
        this.calcBalance();
      });
    }
  }

  public transfer(beneficiary: string, amount: number): void {
    const transaction: Transaction = {
      beneficiary,
      categoryCode: 'red',
      amount: {
        amount: -amount,
        currencyCode: 'EUR'
      },
      date: new Date(),
      type: 'Online Transfer'
    }

    const transactions = [ transaction, ...this.transactions.value ];
    this.localStorage.set(STORAGE_KEY, transactions);
    this.transactions.next(transactions);
    this.calcBalance();
  }

  private calcBalance() {
    this.balance.next(
      this.transactions.value.reduce(
        (total, current) => {
          total.amount += current.amount.amount;
          total.currencyCode = current.amount.currencyCode
          return total;
        },
        { ...ZERO }
      )
    )
  }
}
