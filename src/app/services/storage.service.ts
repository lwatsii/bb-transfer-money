import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CreditDebitIndicator, IncommingTransaction, Transaction } from './model';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor(
    private http: HttpClient
  ) {

  }

  retrieveTransactions(): Observable<IncommingTransaction[]> {
    return this.http.get<IncommingTransaction[]>('assets/mock/transactions.json');
  }

  getTransactions(): Observable<Transaction[]> {
    return this.retrieveTransactions().pipe(
      tap(console.log),
      map(data => data.data.map(this.mapTransaction))
    );
  }

  private mapTransaction(t: IncommingTransaction): Transaction {
    const {
      categoryCode,
      dates: { valueDate },
      transaction: {
        amountCurrency: {
          amount,
          currencyCode
        },
        type,
        creditDebitIndicator
      },
      merchant: { name: beneficiary }
    } = t;
    const transaction: Transaction = {
      date: new Date(valueDate),
      type,
      beneficiary,
      amount: {
        amount: +amount,
        currencyCode
      },
      categoryCode
    };
    if (creditDebitIndicator === 'DBIT') {
      transaction.amount.amount = -transaction.amount.amount;
    }
    return transaction;
  }
}
