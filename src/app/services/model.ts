/**
 * It stores amount and currency together
 */
export interface AmountCurrency {
  amount: number;
  currencyCode: string;
}

export type CreditDebitIndicator = 'CRDT' | 'DBIT';

/**
 * Incomming transaction
 *
 * Example of incomming transaction
 * {
      "categoryCode": "#12a580",
      "dates": {
        "valueDate": 1600493600000
      },
      "transaction": {
        "amountCurrency": {
          "amount": 5000,
          "currencyCode": "EUR"
        },
        "type": "Salaries",
        "creditDebitIndicator": "CRDT"
      },
      "merchant": {
        "name": "Backbase",
        "accountNumber": "SI64397745065188826"
      }
    }
 */
export interface IncommingTransaction {
  categoryCode: string;
  dates: {
    valueDate: number;
  };
  transaction: {
    amountCurrency: {
      amount: string,
      currencyCode: string
    };
    type: string;
    creditDebitIndicator: CreditDebitIndicator;
  };
  merchant: {
    name: string;
  }
}

/**
 * Simplified version of transaction used internally in application.
 * Incomming transactions list is mapped to it.
*/
export interface Transaction {
  date: Date | string;
  type: string;
  beneficiary: string;
  amount: AmountCurrency;
  categoryCode: string;
}
