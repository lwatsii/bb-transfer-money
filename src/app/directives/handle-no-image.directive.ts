import { Directive, Input } from '@angular/core';

/**
 * Displays empty place if error occurs during retrieving image.
 */
@Directive({
  selector: '[appHandleNoImage]',
  host: {
    '[src]': 'src',
    '(error)': 'onError()',
  },
})
export class HandleNoImageDirective {
  @Input()
  src: string;

  private readonly emptyImage: string = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';

  constructor() {}

  public onError() {
    this.src = this.emptyImage;
  }
}
