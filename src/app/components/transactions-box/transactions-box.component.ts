import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Transaction } from 'src/app/services/model';
import { TransactionsService } from 'src/app/services/transactions.service';

enum SortField {
  DATE,
  BENEFICIARY,
  AMOUNT
}

const SORT_ASCENDING = 'asc';
const SORT_DESCENDING= 'desc';
type SortOrder = 'asc' | 'desc';

@Component({
  selector: 'app-transactions-box',
  templateUrl: './transactions-box.component.html',
  styleUrls: ['./transactions-box.component.scss']
})
export class TransactionsBoxComponent implements OnInit, OnDestroy {

  transactions: Transaction[] = [];
  filteredTransactions: Transaction[] = [];

  SortField = SortField;

  sortField: SortField = SortField.AMOUNT;

  sortOrder: SortOrder = SORT_DESCENDING;

  get filterValue() {
    return this._filterValue;
  }
  set filterValue(v : string) {
    this._filterValue = v;
    this.filter();
    this.sort();
  }
  private _filterValue: string = '';

  private allSubscriptions = new Subscription();

  constructor(
    private transactionsService: TransactionsService
  ) {
    this.allSubscriptions.add(
      transactionsService.transactions$.subscribe(transactions => {
        this.transactions = transactions;
        this.sortField = SortField.DATE;
        this.sortOrder = SORT_DESCENDING;
        this.filterValue = '';
        this.filter();
        this.sort();
      })
    );
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    // This is not needed but it is good practice. ngDestroy for this component is called when page is closed
    // so all resource will be freed by browser.
    this.allSubscriptions.unsubscribe();
  }

  filter(): void {
    const filterValue = this.filterValue.toLowerCase();
                                                                                                                                   this.filteredTransactions = [ ...this.transactions ].filter(v => v.beneficiary.toLowerCase().indexOf(filterValue) != -1);
  }

  sortBy(field: SortField): void {
    if (this.sortField === field) {
      this.sortOrder = this.sortOrder === SORT_ASCENDING ? SORT_DESCENDING : SORT_ASCENDING;
    } else {
      this.sortField = field;
    }
    this.sort();
  }

  private sortByAmount({  amount: { amount: amount1 }}: Transaction, {  amount: { amount: amount2 }}: Transaction) {
    return amount1 - amount2;
  }

  private sortByDate( { date: date1 }: Transaction, {  date: date2 }: Transaction) {
    return (date1 as Date).getTime() - (date2 as Date).getTime();
  }

  private sortByBeneficiary( { beneficiary: beneficiary1 }: Transaction, { beneficiary: beneficiary2 }: Transaction) {
    return beneficiary1.localeCompare(beneficiary2);
  }

  sort(): void {
    let sortFunc = null;
    switch(this.sortField) {
      case SortField.AMOUNT: sortFunc = this.sortByAmount; break;
      case SortField.DATE: sortFunc = this.sortByDate; break;
      case SortField.BENEFICIARY: sortFunc = this.sortByBeneficiary; break;
    }
    if (sortFunc) {
      this.filteredTransactions.sort(sortFunc);
      if (this.sortOrder === SORT_DESCENDING) {
        this.filteredTransactions.reverse();
      }
    }
  }

  clearFilter(): void {
    this.filterValue = '';
    this.filter();
    this.sort();
 }
}
