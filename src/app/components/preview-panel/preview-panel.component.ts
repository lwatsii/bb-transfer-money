import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AmountCurrency } from 'src/app/services/model';

@Component({
  selector: 'app-preview-panel',
  templateUrl: './preview-panel.component.html',
  styleUrls: ['./preview-panel.component.scss']
})
export class PreviewPanelComponent implements OnInit {

  @Input()
  beneficiary: string;

  @Input()
  amount: number;

  @Input()
  balance: AmountCurrency;

  @Output()
  cancel = new EventEmitter();

  @Output()
  transfer = new EventEmitter();

  get newBalance(): AmountCurrency {
    return {
      amount: this.balance.amount - this.amount,
      currencyCode: this.balance.currencyCode
    }
  }

  get displayedAmount(): AmountCurrency {
    return {
      amount: this.amount,
      currencyCode: this.balance?.currencyCode ?? ''
    }
  }

  constructor() { }

  ngOnInit(): void {
  }

}
