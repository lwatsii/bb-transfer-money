import { Component, OnDestroy, OnInit } from '@angular/core';
import { timeout } from 'q';
import { Subscription } from 'rxjs';
import { AmountCurrency } from 'src/app/services/model';
import { TransactionsService } from 'src/app/services/transactions.service';

/**
 * Minimal value of balance
 */
const MIN_BALANCE = -500;

@Component({
  selector: 'app-transfer-box',
  templateUrl: './transfer-box.component.html',
  styleUrls: ['./transfer-box.component.scss']
})
export class TransferBoxComponent implements OnInit, OnDestroy {

  balance: AmountCurrency = null;

  private allSubscriptions = new Subscription();

  beneficiary: string;
  amount: number;

  message: string;
  messageTimerId: any;

  previewMode = false;

  constructor(
    private transactionsService: TransactionsService
  ) {

  }

  ngOnInit(): void {
    this.allSubscriptions.add(
      this.transactionsService.balance$.subscribe(balance => {
        this.balance = balance;
      })
    );
    this.reset();
  }

  ngOnDestroy(): void {
    this.allSubscriptions.unsubscribe();
  }

  reset(): void {
    this.amount = 0.0;
    this.beneficiary = '';
  }

  submit(): void {
    if (this.beneficiary.length == 0) {
      this.showMessage("Beneficiary is required");
    } else if (this.balance.amount - this.amount < MIN_BALANCE) {
      this.showMessage("You have no enough money.")
    } else {
      this.previewMode = true;
    }
  }

  transfer(): void {
    this.transactionsService.transfer(this.beneficiary, this.amount);
    this.reset();
    this.previewMode = false;
  }

  cancel(): void {
    this.previewMode = false;
  }

  private showMessage(message: string): void {
    this.message = message;
    if (this.messageTimerId) {
      clearTimeout(this.messageTimerId);
    }
    this.messageTimerId = setTimeout(() => {
      this.message = '';
      this.messageTimerId = null;
    }, 2500)
  }
}
