# Requirements

1. node v14.15.4
2. npm 6.14.10

# Setup

## Install dependencies

    npm install

## Runing application

    npm start

# Developing

## Implementation milestones:

1. Create skeleton of application
	* create empty angular project
	* remove not necessery code created by 'ng new'
2. Implement general layout
3. Implement "Transaction History"
4. Implement "Transfer money"
5. Making application responsible and improvements in layout
6. Bugfixing
7. Additional things:
	* sorting asc/desc

Each milestone is created in own branch. Functionality is merged into master branch when is implemented.

## Structure of application

Application is divied by angular functionality. Each type of angular item is put into own path.

### app/components
It contains all components used by application.
* PreviewPanel - Preview of transaction.
* TransactionsBox - List of all transactions with filtering and sorting functionality.
* TransferBox - Used to fill data before making money transfer.

### app/directives
* HandleNoImage - directive used to show empty placeholder if image is unavailable.

### app/pipes
* AmountCurrencyPipe - pipe to display amount and currency.
* BeneficiaryImagePipe - pipe to create image url based on beneficiary name. It is auxiliary pipe because transation doesn't contain image (like described in documentation). Images are put in assets/beneficiary paths.

### app/services
It contains all services used by application
* TransactionsService - it gets transactions, make a money transfer, retrieve all transactions assigned to user and calculate balance.
* StorageService - it reads transactions from the backend.
* LocalStorage - auxiliary service to support local storing of transactions. Transactions are stored in browser local storage to make them persistent.

### AppComponent
Main component of application. It contains layout skelet.

## Additional information

### Remote repository

The project is available at Bitbucket. To clone it use:

  git clone git@bitbucket.org:lwatsii/bb-transfer-money.git.

It'll be available public until 22.01.2021.

There is branch master with fnal version version of applcaition. Each milestone-* contains implementation steps.

### More information

* Implementation time: 13 hours. Much time has been spent on CSS and adjusting layout.
* It contains only few tests to test retrieving recent transactions.

